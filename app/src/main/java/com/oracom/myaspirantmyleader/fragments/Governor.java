package com.oracom.myaspirantmyleader.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.oracom.myaspirantmyleader.R;
import com.oracom.myaspirantmyleader.activities.HomeActivity;
import com.oracom.myaspirantmyleader.utilities.Globall;
import com.roger.catloadinglibrary.CatLoadingView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;


import dmax.dialog.SpotsDialog;

import static com.oracom.myaspirantmyleader.R.id.webView;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Governor.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Governor#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Governor extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";

    // TODO: Rename and change types of parameters
    public String web_url;

    public AlertDialog dialog;
    CatLoadingView mView;;
    private int progressStatus = 0;
    public static Document doc ;
    public static Elements ele;
    public WebView webView;


    private OnFragmentInteractionListener mListener;

    public Governor() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.

     * @return A new instance of fragment Governor.
     */
    // TODO: Rename and change types and number of parameters
    public static Governor newInstance(String param1) {
        Governor fragment = new Governor();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);


        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            web_url = getArguments().getString(ARG_PARAM1);
        }


    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //((HomeActivity) getActivity()).setWebView(webView);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_governor, container, false);
        dialog = new SpotsDialog(getContext());
        //progressBar = (ProgressBar)rootView.findViewById(R.id.progressBar);
        webView = (WebView)rootView.findViewById(R.id.webView);

        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        webView.getSettings().setJavaScriptEnabled(true);
        HomeActivity.webView = webView;
        webView.setOnKeyListener(new View.OnKeyListener(){
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((keyCode == KeyEvent.KEYCODE_BACK) && webView.canGoBack())
                {
                    webView.goBack();
                    return true;
                }
                return false;
            }
        });
        //new thread





        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon){
                // Do something on page loading started
                // Visible the progressbar
                // mProgressBar.setVisibility(View.VISIBLE);
                dialog.show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        dialog.hide();
                    }
                },7000);
            }
            @Override
            public void onPageFinished(WebView view, String url) {
                //webView.loadUrl("javascript:document.getElementsByClassName('td-header-wrap td-header-style-3')[0].style.display='none'; void(0);");

                //webView.loadUrl("javascript:document.getElementsByClassName('td-header-wrap td-header-style-3')[0].style.display='none'; void(0);");
                //webView.loadUrl("javascript:document.getElementsByClassName('td-pb-span4 td-main-sidebar')[0].style.display='none'; void(0);");
                //webView.loadUrl("javascript:document.getElementsByClassName('td-category-header td-container-wrap')[0].style.display='none'; void(0);");
               //webView.loadUrl("javascript:document.getElementsByClassName('td-footer-wrapper td-container-wrap td-footer-template-14')[0].style.display='none'; void(0);");

            }
        });

        webView.loadUrl(web_url);

        //((HomeActivity) getActivity()).setWebView(webView);






        //loadWebsite(rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //new LoadData().execute();


    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(String uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        //((HomeActivity) getActivity()).setWebView(webView);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(String uri);

    }

    private void loadWebsite(View rootView) {
        final View passedView=rootView;
        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.e("Process","Started Running UI thread");

                try {
                    doc = Jsoup.connect(Globall.NEWS_URL).get();
                    ele = doc.select("div.td-container").first().parents();
                    String html = ele.toString();
                    Log.e("html received", html);
                    String mime = "text/html";

                    String encoding = "utf-8";
                    final WebView webView = (WebView) passedView.findViewById(R.id.webView);
                    WebSettings webSettings = webView.getSettings();
                    webSettings.setJavaScriptEnabled(true);
                    webSettings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
                    //webView.loadData(html, mime, encoding);
                    //webView.loadDataWithBaseURL("", html, mime, encoding, "");



                } catch (IOException e) {
                    Log.e("IO exception", "exception caught");
                }


            }
        }).start();
    }



    private class  LoadData extends AsyncTask<Void,Void,Void>
    {

        String html=new String();
        Document docc = null;
        @Override
        protected Void doInBackground(Void... params) {
            //dialog.show();

            try {

                docc= Jsoup.connect(Globall.NEWS_URL).get();
                ele = docc.select("div.td-container").first().parents();
                String html = ele.toString();
                Log.e("html received", html);
                String mime = "text/html";

                String encoding = "utf-8";
                //dialog.hide();
            } catch (IOException e) {
                e.printStackTrace();
                Log.e("exception", "exception iko");
                dialog.hide();
            }
            //get total document



            return null;
        }
        @Override
        protected void onPostExecute(Void result) {

            super.onPostExecute(result);
            webView.loadDataWithBaseURL(null,html,
                    "text/html", "utf-8", null);
            Log.e("loading..", "Loaded data");

        }
    }







}
