package com.oracom.myaspirantmyleader.utilities;

/**
 * Created by Neville Masheti on 9/20/2017.
 */

public class Globall {
    public static String SELECTED_OPTION  = "";
    public static String OPTIONS_TITLE=" ";
    public  static int CURRENT_FRAG=0;
    public static String HOME_URL  = "https://myaspirantmyleader.co.ke";
    public static String NEWS_URL  = "https://myaspirantmyleader.co.ke/category/general-election-news/";
    public static String TV_URL  = "https://myaspirantmyleader.co.ke/tv/";
    public static String GOVERNORS_URL  = "https://myaspirantmyleader.co.ke/governors";
    public static String HOTSPOTS_URL  = "https://myaspirantmyleader.co.ke/development-hotspots/";
    public static String JT_URL  = "https://myaspirantmyleader.co.ke/ctl-stories/jobs-tenders/";
    public static String SENATORS_URL  = "https://myaspirantmyleader.co.ke/senators/ ";
    public static String WREP_URL  = "https://myaspirantmyleader.co.ke/women-reps-2 ";
    public static String MPS_URL  = "https://myaspirantmyleader.co.ke/mps-leaders";
    public static String CS_URL  = "https://myaspirantmyleader.co.ke/ministries-cabinet-secretaries/ ";
    public static String AGENDA_URL  = "https://myaspirantmyleader.co.ke/ctl-stories/agenda-issues/";
    public static String SERVICES_URL  = "https://myaspirantmyleader.co.ke/campaign-services-solutions/";
    public static String SCORECARD_URL  = "https://myaspirantmyleader.co.ke/submit-news/";
    public static String HOTSPOT_URL  = "https://myaspirantmyleader.co.ke/enquiry-form/";
    public static String PARTNER_URL  = "https://myaspirantmyleader.co.ke/donate-2-2/partner-with-us/";
    public static String APP_SHARE_LINK = "Hey check out, https://goo.gl/J9V7Bh";
    public static String APP_LINK = "market://details?id=com.oracom.myaspirantmyleader";
    public static String APP_LINK_WEB = "https://play.google.com/store/apps/details?id=com.oracom.myaspirantmyleader";
}
