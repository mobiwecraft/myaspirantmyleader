package com.oracom.myaspirantmyleader.utilities;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.oracom.myaspirantmyleader.fragments.Governor;

/**
 * Created by Neville Masheti on 9/28/2017.
 */

public class MyPagerAdapter extends FragmentPagerAdapter {

    private final String[] TITLES = {"News", "TV", "Gov", "Sen", "WREP",
            "MPs", "CS", "Agenda", "Services", "HotSpots", "J&T"};

    public MyPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return TITLES[position];
    }

    @Override
    public int getCount() {
        return TITLES.length;
    }

    @Override
    public Fragment getItem(int position) {
        if(position==0)
        {

            return Governor.newInstance(Globall.NEWS_URL);

        }
        else if(position==1)
        {

            return Governor.newInstance(Globall.TV_URL);
        }
        else if(position==2)
        {

            return Governor.newInstance(Globall.GOVERNORS_URL);
        }
        else if(position==3)
        {

            return Governor.newInstance(Globall.SENATORS_URL);
        }
        else if(position==4)
        {

            return Governor.newInstance(Globall.WREP_URL);
        }
        else if(position==5)
        {

            return Governor.newInstance(Globall.MPS_URL);
        }
        else if(position==6)
        {

            return Governor.newInstance(Globall.CS_URL);
        }
        else if(position==7)
        {

            return Governor.newInstance(Globall.AGENDA_URL);
        }
        else if(position==8)
        {

            return Governor.newInstance(Globall.SERVICES_URL);
        }
        else if(position==9)
        {

            return Governor.newInstance(Globall.HOTSPOTS_URL);
        }
        else if(position==10)
        {

            return Governor.newInstance(Globall.JT_URL);
        }
        else
        {
            return Governor.newInstance(Globall.NEWS_URL);
        }


    }
}
