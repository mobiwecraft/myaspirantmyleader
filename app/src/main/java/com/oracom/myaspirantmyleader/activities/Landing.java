package com.oracom.myaspirantmyleader.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

import java.util.HashMap;

import android.support.v7.widget.CardView;


import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;




import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.oracom.myaspirantmyleader.R;


public class Landing extends AppCompatActivity implements BaseSliderView.OnSliderClickListener {


    CardView cardView;
    Context k;
    private SliderLayout headerSlider;

    ProgressDialog progressDialog;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);
        k=this;
        progressDialog = new ProgressDialog(Landing.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setMessage("Loading...");
        cardView=(CardView)findViewById(R.id.btnProceed);
        headerSlider=(SliderLayout)findViewById(R.id.imgHeader);
        cardView.getCardBackgroundColor().withAlpha(80);
        cardView.setAlpha((float)0.8);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        HashMap<String,Integer> file_maps = new HashMap<String, Integer>();
        file_maps.put("a",R.drawable.slidera);
        file_maps.put("b",R.drawable.sliderc);
        file_maps.put("c",R.drawable.sliderb);
        file_maps.put("d",R.drawable.sliderd);





        cardView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Landing.this, HomeActivity.class);
                startActivity(intent);
                finish();
            }

        });

        for(String name : file_maps.keySet()){
            TextSliderView textSliderView = new TextSliderView(this);
            // initialize a SliderLayout
            textSliderView
                    .image(file_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(this);

            //add your extra information
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra",name);

            headerSlider.addSlider(textSliderView);
        }
        headerSlider.setPresetTransformer(SliderLayout.Transformer.Tablet);
        headerSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        headerSlider.setCustomAnimation(new DescriptionAnimation());
        headerSlider.setDuration(5000);






        //mView = new CatLoadingView();
        //showDialog();



        //set fancy typeface
        //tv.setTypeface(Typefaces.get(this, "Satisfy-Regular.ttf"));

    }


    @Override
    protected void onStop() {
        // To prevent a memory leak on rotation, make sure to call stopAutoCycle() on the slider before activity or fragment is destroyed
        headerSlider.stopAutoCycle();
        super.onStop();
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {
        // Toast.makeText(this,slider.getBundle().get("extra") + "",Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

    }

    @Override
    protected void onPostResume() {
        super.onPostResume();

    }



    @Override
    public void onBackPressed() {
        new MaterialDialog.Builder(Landing.this)
                .title("Logout")
                .content("Do you want to quit the application? ")
                .positiveText("NO")
                .negativeText("QUIT")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {


                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                        finish();

                    }
                })
                .show();
    }



}


