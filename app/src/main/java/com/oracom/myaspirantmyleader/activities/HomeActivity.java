package com.oracom.myaspirantmyleader.activities;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.astuetz.PagerSlidingTabStrip;

import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.oracom.myaspirantmyleader.R;
import com.oracom.myaspirantmyleader.fragments.Governor;
import com.oracom.myaspirantmyleader.utilities.Globall;
import com.oracom.myaspirantmyleader.utilities.MyPagerAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends AppCompatActivity  implements Governor.OnFragmentInteractionListener, View.OnClickListener {


    public Context kcon;
    public static WebView webView;
    private Boolean isFabOpen = false;
    private com.getbase.floatingactionbutton.FloatingActionButton fab_scorecard,fab_partner, fab_hotspot, fab_share, fab_rate;
    private MyPagerAdapter adapter;
    PagerSlidingTabStrip tabLayout;
    @BindView(R.id.container)
    ViewPager pager;

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        kcon =this;
        ButterKnife.bind(this);

        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);
        //getSupportActionBar().setIcon(R.mipmap.ic_launcher);
        final FloatingActionsMenu fabMenu = (FloatingActionsMenu) findViewById(R.id.fab_menu);
        final FrameLayout frameLayout;
        tabLayout = (PagerSlidingTabStrip) findViewById(R.id.tabs);

        adapter = new MyPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(adapter);
        tabLayout.setViewPager(pager);
        pager.setCurrentItem(0);
        fab_hotspot = (com.getbase.floatingactionbutton.FloatingActionButton) findViewById(R.id.fab_hotspot);
        fab_scorecard= (com.getbase.floatingactionbutton.FloatingActionButton)findViewById(R.id.fab_scorecard);
        fab_partner = (com.getbase.floatingactionbutton.FloatingActionButton)findViewById(R.id.fab_partner);
        fab_share = (com.getbase.floatingactionbutton.FloatingActionButton)findViewById(R.id.fab_share);
        fab_rate = (com.getbase.floatingactionbutton.FloatingActionButton)findViewById(R.id.fab_rate);

        frameLayout = (FrameLayout) findViewById(R.id.frame_layout);
        frameLayout.getBackground().setAlpha(0);


        fab_hotspot.setOnClickListener(this);
        fab_partner.setOnClickListener(this);
        fab_scorecard.setOnClickListener(this);
        fab_share.setOnClickListener(this);
        fab_rate.setOnClickListener(this);

        fabMenu.setOnFloatingActionsMenuUpdateListener(new FloatingActionsMenu.OnFloatingActionsMenuUpdateListener() {
            @Override
            public void onMenuExpanded() {
                frameLayout.getBackground().setAlpha(240);
                frameLayout.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        fabMenu.collapse();
                        return true;
                    }
                });


            }

            @Override
            public void onMenuCollapsed() {
                frameLayout.getBackground().setAlpha(0);
                frameLayout.setOnTouchListener(null);

            }
        });




    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        Intent intent;
        switch (id){
            case R.id.fab_partner:
                Globall.SELECTED_OPTION=Globall.PARTNER_URL;
                Globall.OPTIONS_TITLE="Partner With Us";
                intent = new Intent(HomeActivity.this, OptionsActivity.class);
                startActivity(intent);
                break;

            case R.id.fab_hotspot:
                Globall.SELECTED_OPTION=Globall.HOTSPOT_URL;
                Globall.OPTIONS_TITLE="Submit HotSpot";
                intent = new Intent(HomeActivity.this, OptionsActivity.class);
                startActivity(intent);
                break;

            case R.id.fab_scorecard:
                Globall.SELECTED_OPTION=Globall.SCORECARD_URL;
                Globall.OPTIONS_TITLE="Submit ScoreCard";
                intent = new Intent(HomeActivity.this, OptionsActivity.class);
                startActivity(intent);
                break;
            case R.id.fab_rate:
                rateApp();
                break;
            case R.id.fab_share:
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT,Globall.APP_SHARE_LINK);
                sendIntent.setType("text/plain");
                //startActivity(sendIntent);
                startActivity(Intent.createChooser(sendIntent, "Share via"));
                break;

        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_services) {
            Globall.SELECTED_OPTION=Globall.SERVICES_URL;
            Globall.OPTIONS_TITLE="Our Services";
            Intent intent = new Intent(HomeActivity.this, OptionsActivity.class);
            startActivity(intent);
        }
        if (id == R.id.action_rate) {
            return true;
        }
        if (id == R.id.action_share) {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT,Globall.APP_SHARE_LINK);
            sendIntent.setType("text/plain");
            //startActivity(sendIntent);
            startActivity(Intent.createChooser(sendIntent, "Share via"));
        }

        if (id == R.id.action_refresh) {
            adapter = new MyPagerAdapter(getSupportFragmentManager());
            pager.setAdapter(adapter);
            tabLayout.setViewPager(pager);
            pager.setCurrentItem(Globall.CURRENT_FRAG);

        }

        return super.onOptionsItemSelected(item);
    }

    public void onFragmentInteraction(String uri) {

    }

    //On click event for rate this app button
    public void rateApp() {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        //Try Google play
        intent.setData(Uri.parse(Globall.APP_LINK));
        if (!checkIntent(intent)) {
            //Market (Google play) app seems not installed, let's try to open a webbrowser
            intent.setData(Uri.parse(Globall.APP_LINK_WEB));
            if (!checkIntent(intent)) {
                //Well if this also fails, we have run out of options, inform the user.
                Toast.makeText(this, "Could not open Android Play Store, please install the Play Store app.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private boolean checkIntent(Intent aIntent) {
        try
        {
            startActivity(aIntent);
            return true;
        }
        catch (ActivityNotFoundException e)
        {
            return false;
        }
    }

    @Override
    public void onBackPressed()
    {
        if (webView.canGoBack()) {
            webView.goBack();
        } else if (!getFragmentManager().popBackStackImmediate()) {
            super.onBackPressed();
        }
    }

    public void setWebView(WebView web) {
       webView = web;
    }


}
