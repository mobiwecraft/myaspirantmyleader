package com.oracom.myaspirantmyleader.activities;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.oracom.myaspirantmyleader.R;
import com.oracom.myaspirantmyleader.utilities.Globall;
import com.roger.catloadinglibrary.CatLoadingView;

public class OptionsActivity extends AppCompatActivity {
    WebView webView;
    private ProgressBar mProgressBar;
    Context context;
    CatLoadingView mView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_options);
        context=this;
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);
        getSupportActionBar().setTitle(Globall.OPTIONS_TITLE);

        mView = new CatLoadingView();

        webView = (WebView)findViewById(R.id.webView);
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);




        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon){
                // Do something on page loading started
                // Visible the progressbar
               // mProgressBar.setVisibility(View.VISIBLE);
                mView.show(getSupportFragmentManager(), "Loading...");
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                webView.loadUrl("javascript:document.getElementsByClassName('td-header-wrap td-header-style-3')[0].style.display='none'; void(0);");

                webView.loadUrl("javascript:document.getElementsByClassName('td-header-wrap td-header-style-3')[0].style.display='none'; void(0);");
                webView.loadUrl("javascript:document.getElementsByClassName('td-pb-span4 td-main-sidebar')[0].style.display='none'; void(0);");
                webView.loadUrl("javascript:document.getElementsByClassName('td-category-header td-container-wrap')[0].style.display='none'; void(0);");
                webView.loadUrl("javascript:document.getElementsByClassName('td-footer-wrapper td-container-wrap td-footer-template-14')[0].style.display='none'; void(0);");
                //if (progressDialog.isShowing()) {
                // progressDialog.dismiss();
                //}
                //view.loadUrl("javascript:document.getElementById('SITE_HEADERinlineContent').style.display='none'; void(0);");
                //webView.loadUrl("javascript: var con = document.getElementById('SITE_HEADERinlineContent');"+
                // "con.parentNode.removeChild(con);");

                mView.dismiss();


            }

            @Override
            public void onLoadResource(WebView view, String url) {
                super.onLoadResource(view, url);
                //view.loadUrl("javascript:document.getElementById('SITE_HEADERinlineContent').style.display='none'; void(0);");
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {


            }
        });

          /*
            WebView
                A View that displays web pages. This class is the basis upon which you can roll your
                own web browser or simply display some online content within your Activity. It uses
                the WebKit rendering engine to display web pages and includes methods to navigate
                forward and backward through a history, zoom in and out, perform text searches and more.

            WebChromeClient
                 WebChromeClient is called when something that might impact a browser UI happens,
                 for instance, progress updates and JavaScript alerts are sent here.
        */

        webView.setWebChromeClient(new WebChromeClient(){
            /*
                public void onProgressChanged (WebView view, int newProgress)
                    Tell the host application the current progress of loading a page.

                Parameters
                    view : The WebView that initiated the callback.
                    newProgress : Current page loading progress, represented by an integer
                        between 0 and 100.
            */
            public void onProgressChanged(WebView view, int newProgress){
                // Update the progress bar with page loading progress

            }
        });


        webView.loadUrl(Globall.SELECTED_OPTION);
    }

    // Prevent the back-button from closing the app
    @Override
    public void onBackPressed() {
        if(webView.canGoBack()) {
            webView.goBack();
        } else {
            super.onBackPressed();
        }
    }
}
